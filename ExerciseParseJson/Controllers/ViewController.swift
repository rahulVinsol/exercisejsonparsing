//
//  ViewController.swift
//  ExerciseParseJson
//
//  Created by Rahul Rawat on 16/06/21.
//

import UIKit
import Alamofire
import Foundation

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let networkInterfaceProtocol: NetworkInterfaceProtocol = NetworkInterface.shared
    private var contacts = [Contact]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        
        networkInterfaceProtocol.getContacts(completionHandler: { [weak self] contacts in
            guard let self = self else { return }
            self.contacts = contacts
            self.tableView.reloadData()
        })
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TableViewCell.self)) as! TableViewCell
        
        let contact = contacts[indexPath.row]
        cell.setup(with: contact)
        
        return cell
    }
}
