//
//  TableViewCell.swift
//  ExerciseParseJson
//
//  Created by Rahul Rawat on 16/06/21.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(with contact: Contact) {
        name.text = contact.name
        email.text = contact.email
        phone.text = contact.phone.toString
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
