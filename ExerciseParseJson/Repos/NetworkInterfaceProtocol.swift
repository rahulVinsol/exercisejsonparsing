//
//  RemoteRepo.swift
//  ExerciseParseJson
//
//  Created by Rahul Rawat on 16/06/21.
//

import Foundation
import Alamofire

protocol NetworkInterfaceProtocol {
    func getContacts(completionHandler: @escaping ([Contact]) -> Void)
}

struct NetworkInterface: NetworkInterfaceProtocol {
    static let shared: NetworkInterface = NetworkInterface()
    
    private init() { }
    
    private let baseUrl = "https://api.androidhive.info/"
    
    func getContacts(completionHandler: @escaping ([Contact]) -> Void) {
        Alamofire.AF.request("\(baseUrl)contacts/", method: .get)
            .responseJSON { response in
                if response.data != nil {
                    do {
                        let contacts = try JSONDecoder().decode(ContactsContainer.self, from: response.data!).contacts
                        completionHandler(contacts)
                    } catch {
                        completionHandler([])
                    }
                }
            }
    }
}
