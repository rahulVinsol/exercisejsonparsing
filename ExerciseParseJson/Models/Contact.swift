//
//  Contact.swift
//  ExerciseParseJson
//
//  Created by Rahul Rawat on 16/06/21.
//

import Foundation

struct ContactsContainer: Codable {
    let contacts: [Contact]
}

struct Contact: Codable {
    let id, name, email, address: String
    let gender: Gender
    let phone: Phone
}

enum Gender: String, Codable {
    case female = "female"
    case male = "male"
}

struct Phone: Codable {
    let mobile, home, office: String?
    
    var toString: String {
        get {
            mobile ?? home ?? office ?? ""
        }
    }
}
